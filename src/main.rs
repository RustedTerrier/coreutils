use std::env::args;
mod bin;
fn main() {
    let cmd: Cmd = Command::new(parse_args());
    bin::run(cmd);
}

fn parse_args() -> Vec<String> {
    let mut args: Vec<String> = args().collect();
    args.remove(0);
    args
}

pub struct Cmd {
    pub cmd:  String,
    pub args: Vec<String>
}

pub trait Command {
    fn new(args: Vec<String>) -> Self;
}

impl Command for Cmd {
    fn new(args: Vec<String>) -> Cmd {
        if args.len() == 0 {
            println!("Xerix Coreutils: v{}", env!("CARGO_PKG_VERSION"));
            std::process::exit(0);
        }
        let cmd = args[0].clone();
        let mut args = args;
        args.remove(0);
        Cmd { cmd, args }
    }
}
