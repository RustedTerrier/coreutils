pub mod ls;
pub mod pwd;
use crate::Cmd;

pub fn run(cmd: Cmd) {
    match &cmd.cmd[..] {
        | "ls" => ls::ls(cmd.args),
        | "pwd" => pwd::pwd(cmd.args),
        | _ => {
            std::process::exit(1);
        }
    }
}
