use std::{env::var, fs, process::exit};

pub fn pwd(args: Vec<String>) {
    let pwd = parse_args(args);
    let dir = match pwd.output {
        // env::current_dir() follows symlinks
        | Style::Logical => match var("PWD") {
            | Ok(a) => std::path::Path::new(&a).to_path_buf(),
            | Err(e) => {
                eprintln!("Could not read current dir: {}", e);
                exit(4);
            }
        },
        | Style::Physical => {
            println!("memes");
            match var("PWD") {
                | Ok(a) => match fs::canonicalize(a) {
                    | Ok(b) => b,
                    | Err(e) => {
                        eprintln!("Could not canonicalize directory: {}", e);
                        exit(5);
                    }
                },
                | Err(e) => {
                    eprintln!("Could not read current dir: {}", e);
                    exit(4);
                }
            }
        }
    };
    println!("{:?}", dir);
}

struct Pwd {
    output: Style
}

enum Style {
    Logical,
    Physical
}

fn parse_args(args: Vec<String>) -> Pwd {
    let mut cmd = Pwd { output: Style::Logical };
    for i in args {
        if &i[0 .. 1] != "-" {
            eprintln!("Unknown argument: {}.", i);
            exit(3)
        } else {
            match &i[1 .. 2] {
                | "L" => cmd.output = Style::Logical,
                | "P" => cmd.output = Style::Physical,
                | "-" => match &i[2 ..] {
                    | "logical" => cmd.output = Style::Logical,
                    | "physical" => cmd.output = Style::Physical,
                    | _ => {
                        eprintln!("Unknown flag: {}", i);
                        exit(3);
                    }
                },
                | _ => {
                    eprintln!("Unknown flag: {}", i);
                    exit(3);
                }
            }
        }
    }
    cmd
}

fn main() {}
